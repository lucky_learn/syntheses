# MAKE

> The `make` command will **build** the pdf and then **clean** files.

# MAKE BUILD

> The `make build` command will **build** the pdf

# MAKE CLEAN

> The `make clean` command will **clean** files.